pub struct Event<T> {
    pub wrapped: T,
}
impl<T> std::hash::Hash for Event<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        "".hash(state);
    }
}
impl<T> PartialEq for Event<T> {
    fn eq(&self, _: &Self) -> bool {
        true
    }
}
impl<T> Eq for Event<T> {}
