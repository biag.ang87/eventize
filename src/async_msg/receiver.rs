use futures::FutureExt;

pub type Abort = ();

pub struct Receiver<'r, M>
where
    M: protobuf::Message,
{
    istream: protobuf::CodedInputStream<'r>,
    recv_msg_tx: std::sync::mpsc::Sender<M>,
    recv_err_tx: std::sync::mpsc::Sender<protobuf::ProtobufError>,
    abort_rx: std::sync::mpsc::Receiver<Abort>,
}

impl<'r, M> Receiver<'r, M>
where
    M: protobuf::Message,
{
    pub fn new<R>(
        reader: &'r mut R,
        recv_msg_tx: std::sync::mpsc::Sender<M>,
        recv_err_tx: std::sync::mpsc::Sender<protobuf::ProtobufError>,
        abort_rx: std::sync::mpsc::Receiver<Abort>,
    ) -> Receiver<'r, M>
    where
        R: std::io::Read,
    {
        Receiver {
            istream: protobuf::CodedInputStream::new(reader),
            recv_msg_tx,
            recv_err_tx,
            abort_rx,
        }
    }

    pub async fn run(&mut self) {
        loop {
            let read_msg_fut = async { self.istream.read_message() }.fuse();
            let mut read_msg_fut = Box::pin(read_msg_fut);
            let abort_fut = async { self.abort_rx.recv() }.fuse();
            let mut abort_fut = Box::pin(abort_fut);
            futures::select! {
                read_msg_result = read_msg_fut => match read_msg_result {
                    Ok(msg) => self.recv_msg_tx.send(msg).unwrap(),
                    Err(e) => self.recv_err_tx.send(e).unwrap(),
                },
                abort_result = abort_fut => match abort_result {
                    _ => break
                }
            };
        }
    }
}
