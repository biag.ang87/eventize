use futures::FutureExt;

pub type Abort = ();

pub enum Error {
    Proto(protobuf::ProtobufError),
    Channel(std::sync::mpsc::RecvError),
}

pub struct Sender<'w, M>
where
    M: protobuf::Message,
{
    ostream: protobuf::CodedOutputStream<'w>,
    send_msg_rx: std::sync::mpsc::Receiver<M>,
    send_err_tx: std::sync::mpsc::Sender<Error>,
    abort_rx: std::sync::mpsc::Receiver<Abort>,
}

impl<'w, M> Sender<'w, M>
where
    M: protobuf::Message,
{
    pub fn new<W>(
        writer: &'w mut W,
        send_msg_rx: std::sync::mpsc::Receiver<M>,
        send_err_tx: std::sync::mpsc::Sender<Error>,
        abort_rx: std::sync::mpsc::Receiver<Abort>,
    ) -> Sender<'w, M>
    where
        W: std::io::Write,
    {
        Sender {
            ostream: protobuf::CodedOutputStream::new(writer),
            send_msg_rx,
            send_err_tx,
            abort_rx,
        }
    }

    pub async fn run(&mut self) {
        loop {
            let write_msg_fut = async { self.send_msg_rx.recv() }.fuse();
            let mut write_msg_fut = Box::pin(write_msg_fut);
            let abort_fut = async { self.abort_rx.recv() }.fuse();
            let mut abort_fut = Box::pin(abort_fut);
            futures::select! {
                write_msg_result = write_msg_fut => match write_msg_result {
                    Ok(msg) => match msg.write_length_delimited_to(&mut self.ostream) {
                        Ok(_) => {},
                        Err(e) => self.send_err_tx.send(Error::Proto(e)).unwrap(),
                    },
                    Err(e) => self.send_err_tx.send(Error::Channel(e)).unwrap(),
                },
                abort_result = abort_fut => match abort_result {
                    _ => break
                }
            };
        }
    }
}
