pub type Sending<M> = Result<M, ()>;

pub struct Sender<'w, M: protobuf::Message> {
    ostream: protobuf::CodedOutputStream<'w>,
    send_msg_rx: std::sync::mpsc::Receiver<Sending<M>>,
}

impl<'w, M: protobuf::Message> Sender<'w, M> {
    pub fn new<W: std::io::Write>(
        writer: &'w mut W,
        send_msg_rx: std::sync::mpsc::Receiver<Sending<M>>,
    ) -> Sender<'w, M> {
        Sender {
            ostream: protobuf::CodedOutputStream::new(writer),
            send_msg_rx,
        }
    }

    pub fn run(&mut self) {
        loop {
            match self.send_msg_rx.recv() {
                Ok(sending) => match sending {
                    Ok(msg) => match msg.write_length_delimited_to(&mut self.ostream) {
                        Ok(_) => match self.ostream.flush() {
                            Ok(_) => {}
                            Err(_) => break,
                        },
                        Err(_) => break,
                    },

                    Err(_) => break,
                },
                Err(_) => break,
            }
        }
    }
}
