pub type Received<M> = protobuf::ProtobufResult<M>;

pub struct Receiver<'r, M: protobuf::Message> {
    istream: protobuf::CodedInputStream<'r>,
    recv_msg_tx: std::sync::mpsc::Sender<Received<M>>,
}

impl<'r, M: protobuf::Message> Receiver<'r, M> {
    pub fn new<R: std::io::Read>(
        reader: &'r mut R,
        recv_msg_tx: std::sync::mpsc::Sender<Received<M>>,
    ) -> Receiver<'r, M> {
        Receiver {
            istream: protobuf::CodedInputStream::new(reader),
            recv_msg_tx,
        }
    }

    pub fn run(&mut self) {
        loop {
            let received = self.istream.read_message();
            match received {
                Ok(_) => self.recv_msg_tx.send(received).unwrap(),
                Err(_) => {
                    self.recv_msg_tx.send(received).unwrap_or_default();
                    break;
                }
            }
        }
    }
}
